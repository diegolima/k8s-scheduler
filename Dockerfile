FROM google/cloud-sdk:alpine
RUN gcloud components install kubectl
WORKDIR /app 
COPY . .
RUN chmod +x "./execute.sh"
ENTRYPOINT ["./execute.sh"]
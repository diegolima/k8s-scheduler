#!/bin/bash
PERIOD=$1
NAMESPACE=$2
HPAS=$3

test -z $HPAS && HPAS=$(kubectl -n $NAMESPACE get hpa --no-headers -o custom-columns=":metadata.name")
for HPA in $HPAS; do
    HPA_MIN_REPLICAS=$(kubectl -n $NAMESPACE get hpa $HPA --no-headers -o custom-columns=":metadata.labels.min_replicas_$PERIOD")
    HPA_MAX_REPLICAS=$(kubectl -n $NAMESPACE get hpa $HPA --no-headers -o custom-columns=":metadata.labels.max_replicas_$PERIOD")
    kubectl -n $NAMESPACE patch hpa $HPA --patch "{\"spec\":{\"minReplicas\":$HPA_MIN_REPLICAS, \"maxReplicas\":$HPA_MAX_REPLICAS}}"
done